/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.repositories;

import com.mycompany.graph.model.MailAddress;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 *
 * @author jantunea
 */
public interface MailAddressRepository extends GraphRepository<MailAddress>{
    
}
