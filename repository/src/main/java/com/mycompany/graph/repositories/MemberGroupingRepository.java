/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.repositories;

import com.mycompany.graph.model.GroupBase;
import com.mycompany.graph.model.MemberGrouping;
import java.util.List;
import org.springframework.data.neo4j.repository.GraphRepository;


public interface MemberGroupingRepository extends GraphRepository<MemberGrouping>{
    
    List<MemberGrouping> findByMyGroup(GroupBase group);
}
