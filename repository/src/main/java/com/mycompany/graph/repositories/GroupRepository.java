/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.repositories;

import com.mycompany.graph.model.GroupBase;
import org.springframework.data.neo4j.repository.GraphRepository;


public interface GroupRepository extends GraphRepository<GroupBase>{
    
}
