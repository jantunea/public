/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.repositories;

import com.mycompany.graph.model.User;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 *
 * @author justmecoding
 */
public interface UserRepository extends GraphRepository<User>{
    
    Iterable<User> findByFamilyName(String name);
    Iterable<User> findByFamilyNameLike(String name);
    User findByUsername(String username);
}
