/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.repositories;

import com.mycompany.graph.model.User;
import javax.annotation.PreDestroy;
import javax.validation.ValidationException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * This is good place to verify SDN behavior.
 * @author justmecoding
 */
@ContextConfiguration(locations = "classpath:/spring/repository-context-embedded.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryEmbeddedTest {
    final static Logger log = LoggerFactory.getLogger(UserRepositoryEmbeddedTest.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private Neo4jTemplate template;

    @Before
    public void cleanUpGraph() {
        template.query("START n = node(*) OPTIONAL MATCH n-[r]-() WHERE ID(n)>0 DELETE n, r", null);
        //Neo4jHelper.cleanDb(template);
    }
    
    @PreDestroy
    public void finalClean() {
        template.query("START n = node(*) OPTIONAL MATCH n-[r]-() WHERE ID(n)>0 DELETE n, r", null);
    }

    @Test
    @Transactional
    public void shouldAllowUserCreation() throws Exception {
        User user = new User("username", "password", "first", "lastname");
        userRepository.save(user);
        assertEquals(1, userRepository.count());
    }
    
    @Transactional
    public void createUser(User user) {
        userRepository.save(user);
    }
    
    @Transactional
    public void assertNumUsersInDb(int numUsers) {
        assertEquals(numUsers,userRepository.count());
    }
    
    @Test(expected = ValidationException.class)
    public void shouldFailWithTooShortVariable() throws Exception {
        assertNumUsersInDb(0);
        User user = new User("us", "password", "first", "lastname");
        try {
            createUser(user);
        } catch (ValidationException e) {
            assertNumUsersInDb(0);
            throw e;
        }
    }
    
    @Test 
    @Transactional
    public void duplicateSaveOnUniqueIndexShouldCreateOnlyOne() {
        User user = new User("username", "password", "first", "lastname");
        userRepository.save(user);
        User user2 = new User("username", "password2", "first2", "lastname2");
        userRepository.save(user2);
        assertEquals(1, userRepository.count());
        
        // but what data it is for other fields? 
        // A: it updates the other fields automatically, nasty thing
        User savedUser = userRepository.findByUsername("username");
        assertEquals("password2", savedUser.getPassword());
        log.debug(savedUser.toString());
    }
    
    @Test(expected = DataIntegrityViolationException.class)
    @Transactional
    public void testChangingAttrToDuplicateEntryInUniqueIndex() {
        User user = new User("username", "password", "first", "lastname");
        userRepository.save(user);
        User user2 = new User("tmpname", "password2", "first2", "lastname2");
        userRepository.save(user2);
        assertEquals(2, userRepository.count());
        
        user2.setUsername("username");
        user2.setPassword("whatshappening");
        User getOrCreated = userRepository.save(user2);
    }
}
