/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.graph.services;

import com.mycompany.graph.model.User;
import com.mycompany.graph.repositories.UserRepository;
import static com.mycompany.graph.services.RepositoryServiceException.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author justmecoding
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public long getNumberOfUsers() {
        return userRepository.count();
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public User createUser(String username, String password, String givenName, String familyName) throws RepositoryServiceException {
        // for performance point, check first if username is reserved. this does not guarantee it is free later in the code
        // but tmp user is not created in vain.
//        if (userRepository.findByUsername(username) != null) {
//            throw new RepositoryServiceException(Code.DUPLICATE_USERNAME, String.format("Duplicate Username: {0}", username));
//        }
        // create temp username so that save creates always new node entity. use mailVerificationString as username
        User tmpUser = new User("temp", password, givenName, familyName);
        tmpUser.setUsername(tmpUser.getMailVerificationString());
        User user = userRepository.save(tmpUser);   
        try {
            user.setUsername(username);
            User modifiedUser = userRepository.save(user);
            return modifiedUser;
        } catch (DataIntegrityViolationException e) {
            log.debug("duplicate username {} temp user {} will be deleted", username, tmpUser.getUsername());
            //userRepository.delete(user.getId());
            throw new RepositoryServiceException(Code.DUPLICATE_USERNAME, String.format("Duplicate Username: {0}", username));
        }
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public Iterable<User> findUsersByFamilyName(String familyName) {
        return userRepository.findByFamilyName(familyName);
    }
    
    @Override
    public Iterable<User> findUsersByGivenName(String givenName) {
        return userRepository.findAllBySchemaPropertyValue("givenName", givenName);
    }
    
    @Override
    public Iterable<User> findUserByUsername(String username) {
        return userRepository.findAllBySchemaPropertyValue("username", username);
    }
    
    @Override
    public Iterable<User> queryUser(String str) {
        return userRepository.findByFamilyNameLike(str);
    }
}
