/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.EntityAuditInfo;
import com.mycompany.graph.model.NodeEntityBase;
import org.springframework.data.neo4j.repository.CRUDRepository;


public abstract class GraphServiceUtils {
    
    private GraphServiceUtils() {
    }
    
    public static void checkNodeType(NodeEntityBase node, Class<?> type,
            CRUDRepository<? extends NodeEntityBase> repository, Long idTried) throws
            ObjectNotFoundException {
        if (node == null) {
            throw new ObjectNotFoundException(String.format("Node %s was not found", idTried));
        }
        if (!repository.getStoredJavaType(node).equals(type)) {
            throw new ObjectNotFoundException(String.format("Node %s was not correct type %s", node.getId(), type.getCanonicalName()));
        }
    }
    
    public static void checkNodeType(EntityAuditInfo node, Class<?> type,
            CRUDRepository<? extends EntityAuditInfo> repository, Long idTried, boolean allowDeleted) throws
            ObjectNotFoundException {
        if (node == null) {
            throw new ObjectNotFoundException(String.format("Node %s was not found", idTried));
        }
        if (!repository.getStoredJavaType(node).equals(type)) {
            throw new ObjectNotFoundException(String.format("Node %s was not correct type %s", node.getId(), type.getCanonicalName()));
        }
        if (!allowDeleted && node.getDeleted()) {
            throw new ObjectNotFoundException(String.format("Node %s was found, but is marked as deleted", idTried));
        }
    }
}
