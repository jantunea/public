/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.GroupBase;
import com.mycompany.graph.model.MemberGrouping;
import com.mycompany.graph.repositories.GroupRepository;
import com.mycompany.graph.repositories.MemberGroupingRepository;
import static com.mycompany.graph.services.GraphServiceUtils.checkNodeType;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class GroupServiceImpl implements GroupService {
    
    private static final Logger LOG = LoggerFactory.getLogger(GroupServiceImpl.class);
    
    @Autowired
    private GroupRepository groupRepo;
    @Autowired
    private MemberGroupingRepository memberGroupingRepo;
    
    @Override
    public GroupBase findOne(Long id) throws ObjectNotFoundException {
        GroupBase group = groupRepo.findOne(id);
        if (group == null) {
            throw new ObjectNotFoundException(objectNotFoundMesg(id));
        }
        return group;
    }

    
    @Override
    public MemberGrouping saveMemberGrouping(MemberGrouping grouping) {
        return memberGroupingRepo.save(grouping);
    }

    private String objectNotFoundMesg(Long id) {
        return String.format("Object %d was not found", id);
    }
    
    @Override
    public MemberGrouping getMemberGrouping(Long id) throws ObjectNotFoundException {
        MemberGrouping grouping = memberGroupingRepo.findOne(id);
        checkNodeType(grouping, MemberGrouping.class, memberGroupingRepo, id);
        return grouping;
    }
    
    @Override
    public List<MemberGrouping> getAllMemberGroupings(GroupBase group) {
        return memberGroupingRepo.findByMyGroup(group);
    }
    
    @Override
    public void deleteMemberGrouping(Long id) {
        memberGroupingRepo.delete(id);
    }
}
