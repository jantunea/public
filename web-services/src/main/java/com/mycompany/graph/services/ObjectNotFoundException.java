/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;


public class ObjectNotFoundException extends MyServiceException {

    public ObjectNotFoundException() {
        super(StatusCode.OBJECT_NOT_FOUND);
    }

    public ObjectNotFoundException(String message) {
        super(StatusCode.OBJECT_NOT_FOUND,message);
    }  
}
