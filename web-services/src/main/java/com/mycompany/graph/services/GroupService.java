/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;


import com.mycompany.graph.model.GroupBase;
import com.mycompany.graph.model.MemberGrouping;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;

@Validated
public interface GroupService {
    
    @Valid GroupBase findOne(Long id) throws ObjectNotFoundException;
    
    // member grouping methods
    MemberGrouping saveMemberGrouping(@Valid MemberGrouping grouping);
    @Valid MemberGrouping getMemberGrouping(Long id) throws ObjectNotFoundException;
    @Valid List<MemberGrouping> getAllMemberGroupings(@Valid GroupBase group);
    void deleteMemberGrouping(Long id);
    
}
