/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.NodeEntityBase;
import java.util.HashSet;
import java.util.Set;

public class MyServiceException extends Exception {
    
    private StatusCode statusCode;

    private final Set<NodeEntityBase> createdInTransaction = new HashSet<>();

    public MyServiceException() {
        this.statusCode = StatusCode.TEST;
    }
    
    public MyServiceException(StatusCode code) {
        this.statusCode = code;
    }

    public MyServiceException(StatusCode code, String message) {
        super(message);
        this.statusCode = code;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }
    
    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public Set<NodeEntityBase> nodesCreatedInTransaction() {
        return createdInTransaction;
    }
}
