/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

/**
 *
 * @author justmecoding
 */
public class RepositoryServiceException extends Exception {
    public enum Code {UNKNOWN_ERROR, VALIDATION_ERROR, DUPLICATE_USERNAME}
    
    private final Code code;

    public RepositoryServiceException(Code code) {
        this.code = code;
    }

    public RepositoryServiceException(Code code, String message) {
        super(message);
        this.code = code;
    }

    public Code getCode() {
        return code;
    }
}
