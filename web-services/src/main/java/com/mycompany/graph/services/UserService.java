/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.User;
/**
 *
 * @author justmecoding
 */
public interface UserService {

    User createUser(String username, String password, String givenName, String familyName) throws RepositoryServiceException;

    User findUserById(Long id);

    Iterable<User> findUserByUsername(String username);

    Iterable<User> findUsersByFamilyName(String familyName);

    Iterable<User> findUsersByGivenName(String givenName);

    long getNumberOfUsers();

    Iterable<User> queryUser(String str);
    
}
