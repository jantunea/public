/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.rest;

import com.mycompany.graph.model.User;
import com.mycompany.graph.services.RepositoryServiceException;
import com.mycompany.graph.services.UserService;
import javax.validation.ValidationException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author justmecoding
 */
@Path("test")
@Component
public class Test {
    final static Logger log = LoggerFactory.getLogger(Test.class);
    
    @Autowired
    UserService userService;
    
    @Path("count")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getNumUsers() {
        return String.valueOf(userService.getNumberOfUsers());
        
    }

    /**
     *
     * @param username
     * @param password
     * @return
     */
    @Path("/user-create/{username}/{password}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String createTestUser(@PathParam("username") String username, @PathParam("password") String password) {
        try {
            User user = userService.createUser(username, password, "First", "FamilyName");
            return user.toString();
        } catch (ValidationException e) {
            // TODO check what e holds
            log.info("validatoin exception ", e.getMessage());
            return e.getMessage();
        } catch (RepositoryServiceException e) {
            return e.getMessage() + " code: " + e.getCode().toString();
        } catch (RuntimeException e) {
            log.error("Unexpected Exception", e);
            return e.getMessage();
            
        }
    }
}
