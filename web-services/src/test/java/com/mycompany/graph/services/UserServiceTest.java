/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.SpecialMailAddr;
import com.mycompany.graph.model.User;
import com.mycompany.graph.repositories.MailAddressRepository;
import com.mycompany.graph.repositories.UserRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.validation.ValidationException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author justmecoding
 */
@ContextConfiguration(locations = "classpath:/spring/repository-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {
    final static Logger log = LoggerFactory.getLogger(UserServiceTest.class);
    
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private MailAddressRepository mailRepo;
    
    @Autowired
    private Neo4jTemplate template;

//    @Rollback(false)
//    @BeforeTransaction
    @Before
    public void cleanUpGraph() {
        template.query("START n = node(*) OPTIONAL MATCH n-[r]-() WHERE ID(n)>0 DELETE n, r", null);
        // cleanDb does not work
        //Neo4jHelper.cleanDb(template);
    }
    
    @Test 
    public void inheritedEntityIsNotFetchedCorrectly() {
        User userToSave = new User("foobar@com", "password", "FirstName", "LastName");
        SpecialMailAddr addr = new SpecialMailAddr();
        addr.setAddress("foobar@com");
        addr.setSpecial("justTest");
        //mailRepo.save(addr);  // not needed
        userToSave.setMailAddress(addr);
        userToSave = userRepo.save(userToSave);
        
        User fetchedUser = userService.findUserById(userToSave.getId());
        SpecialMailAddr fetchedAddr = (SpecialMailAddr) fetchedUser.getMailAddress();
        assertNotNull(fetchedAddr.getSpecial());
    }
    
    @Test
    public void shouldAllowDirectUserCreation() throws Exception {
        assertEquals(0, userService.getNumberOfUsers());
        User myUser = userService.createUser("alextest", "password1", "Alex", "Testaa");
        assertEquals(1, userService.getNumberOfUsers());
        User fromDb = userService.findUserById(myUser.getId());
        assertEquals(myUser.getGivenName(), fromDb.getGivenName());
    }
    
    @Test
    public void specialCharactersShouldBeTreatedDifferently() throws Exception {
        assertEquals(0, userService.getNumberOfUsers());
        User myUser = userService.createUser("alextest", "password1", "Alex", "Testaa");
        User special = userService.createUser("alextest2", "password2", "Älex", "Testää");
        
        Iterator test = userService.findUsersByGivenName("Älex").iterator();
        test.next();
        assertFalse(test.hasNext());
        
        Iterable<User> qresult = userService.queryUser("Testa*");
        Iterator testq = qresult.iterator();
        testq.next();
        assertFalse(testq.hasNext());
    }
    
    @Test
    public void shouldFindByFamilyName() throws Exception {
        makeSomeUsers();
        Iterable<User> resultTestaaja = userService.findUsersByFamilyName("Testaaja");
        User teppo = userService.findUsersByGivenName("Teppo").iterator().next();
        int count = 0;
        for (User u : resultTestaaja) {
            count++;
            if (!u.getId().equals(teppo.getId())) {
                // if "Testaaja" was not Teppo, then it must be "Alpo"
                assertEquals("Alpo", u.getGivenName());
            }
        }
        assertEquals(2, count);
    }    
    
    @Test
    public void shouldFindBySearching() throws Exception {
        makeSomeUsers();
        Iterable<User> result = userService.queryUser("Test*");
        int count = 0;
        for (User u : result) {
            count++;
            log.debug(u.toString());
        }
        assertEquals(3, count);
    }
    
    public void assertNumUsersInDb(int numUsers) {
        assertEquals(numUsers,userService.getNumberOfUsers());
    }
    
    @Test(expected = ValidationException.class)
    public void bothNamesMustBeGiven() throws Exception {
        assertNumUsersInDb(0);
        User user = null;
        try {
            user = userService.createUser("username", "password1", "Firstname", null);
        } catch (ValidationException e) {
            assertNull(user);
            // ERROR, with embedded case, user is not inserted but with neo4j-rest it is.
            assertNumUsersInDb(1);
            throw e;
        }
    }
    
    @Test(expected = ValidationException.class)
    public void tooShortFirstNameNotAllowed() throws Exception {
        assertNumUsersInDb(0);
        User user = null;
        try {
            user = userService.createUser("username", "password1", "F", "FamilyName");
        } catch (ValidationException e) {
            assertNull(user);
            // ERROR, with embedded case, user is not inserted but with neo4j-rest it is.
            // the first save fails in userService, but it is still inserted.
            assertNumUsersInDb(1);
            throw e;
        }
    }
    
    @Test(expected = ValidationException.class)
    public void tooShortUsernameNotAllowed() throws Exception {
        assertNumUsersInDb(0);
        User user = null;
        try {
            user = userService.createUser("u", "password1", "First", "FamilyName");
        } catch (ValidationException e) {
            assertNull(user);
            // Transactions and rollback do not work here, because temp user is created first (and it passes).
            // This is expected
            assertNumUsersInDb(1);
            throw e;
        }
    }
    
    @Test(expected = ValidationException.class)
    public void tooShortPasswordIsNotAllowed() throws Exception {
        User user = userService.createUser("alextest", "passwor", "Alex", "Testaa");
        assertNull(user);
    }
    
    @Test(expected = RepositoryServiceException.class)
    public void usernameMustBeUnique() throws Exception {
        User tmp = userService.createUser("alextest", "password", "Alex", "Testaa");
        log.debug("Older user is {}", tmp);
        try {
            User user = userService.createUser("alextest", "passuword", "Alx", "Tst");
        } catch (RepositoryServiceException e) {
            // make check that there is only one node.
            // ERROR There are two nodes, but it is expected here, as transaction do not work.
            assertEquals(2, userService.getNumberOfUsers());
            throw e;
        }
    }
    
    private Collection<User> makeSomeUsers() throws Exception {
        Collection<User> users = new ArrayList<>();
        users.add(userService.createUser("user1", "password", "Teppo", "Testaaja"));
        users.add(userService.createUser("user2", "password", "Teivo", "Hinaaja"));
        users.add(userService.createUser("user3", "password", "Teemu", "Testing"));
        users.add(userService.createUser("user4", "password", "Ville", "Vitsi"));
        users.add(userService.createUser("user5", "password", "Ana", "Stenval"));
        users.add(userService.createUser("user6", "password", "Antti", "Jeejee"));
        users.add(userService.createUser("user7", "password", "Alpo", "Testaaja"));
        return users;
    }
}
