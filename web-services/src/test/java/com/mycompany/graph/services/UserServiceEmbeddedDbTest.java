/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.SpecialMailAddr;
import com.mycompany.graph.model.User;
import com.mycompany.graph.repositories.UserRepository;
import javax.validation.ValidationException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 */
@ContextConfiguration(locations = "classpath:/spring/repository-context-embedded.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceEmbeddedDbTest {
    final static Logger log = LoggerFactory.getLogger(UserServiceEmbeddedDbTest.class);
    
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private Neo4jTemplate template;

    @Before
    @Transactional
    public void cleanUpGraph() {
        template.query("START n = node(*) OPTIONAL MATCH n-[r]-() WHERE ID(n)>0 DELETE n, r", null);
        //Neo4jHelper.cleanDb(template);
    }
    
    @Test 
    public void inheritedEntityIsNotFetchedCorrectly() {
        User userToSave = new User("foobar@com", "password", "FirstName", "LastName");
        SpecialMailAddr addr = new SpecialMailAddr();
        addr.setAddress("foobar@com");
        addr.setSpecial("justTest");
        //mailRepo.save(addr);  // not needed
        userToSave.setMailAddress(addr);
        userToSave = userRepo.save(userToSave);
        
        User fetchedUser = userService.findUserById(userToSave.getId());
        SpecialMailAddr fetchedAddr = (SpecialMailAddr) fetchedUser.getMailAddress();
        assertNotNull(fetchedAddr.getSpecial());
    }
    
    @Test
    public void shouldAllowDirectUserCreation() throws Exception {
        assertEquals(0, userService.getNumberOfUsers());
        User myUser = userService.createUser("username", "password1", "First", "Familyname");
        assertEquals(1, userService.getNumberOfUsers());
        User fromDb = userService.findUserById(myUser.getId());
        assertEquals(myUser.getGivenName(), fromDb.getGivenName());
    }
    
    public void assertNumUsersInDb(int numUsers) {
        assertEquals(numUsers,userService.getNumberOfUsers());
    }
    
    @Test(expected = ValidationException.class)
    public void bothNamesMustBeGiven() throws Exception {
        assertNumUsersInDb(0);
        User user = null;
        try {
            user = userService.createUser("username", "password1", "Firstname", null);
        } catch (ValidationException e) {
            assertNull(user);
            assertNumUsersInDb(0);
            throw e;
        }
    }
    
    @Test(expected = ValidationException.class)
    public void tooShortFirstNameNotAllowed() throws Exception {
        assertNumUsersInDb(0);
        User user = null;
        try {
            user = userService.createUser("username", "password1", "F", "FamilyName");
        } catch (ValidationException e) {
            assertNull(user);
            assertNumUsersInDb(0);
            throw e;
        }
    }
    
    @Test(expected = ValidationException.class)
    public void tooShortUsernameNotAllowed() throws Exception {
        assertNumUsersInDb(0);
        User user = null;
        try {
            user = userService.createUser("u", "password1", "First", "FamilyName");
        } catch (ValidationException e) {
            assertNull(user);
            // ERROR, should not be INSERTED
            assertNumUsersInDb(1);
            throw e;
        }
    }
    
    @Test(expected = RepositoryServiceException.class)
    public void usernameMustBeUnique() throws Exception {
        User tmp = userService.createUser("username", "password1", "First1", "Familyname1");
        log.debug("Older user is {}", tmp);
        try {
            User user = userService.createUser("username", "password2", "First2", "Familyname2");
        } catch (RepositoryServiceException e) {
            // ERROR HERE, 2 nodes! rollback do not work
            assertNumUsersInDb(2);
            throw e;
        }
    }
}
