/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.services;

import com.mycompany.graph.model.GroupBase;
import com.mycompany.graph.model.MemberGrouping;
import com.mycompany.graph.model.Team;
import com.mycompany.graph.repositories.TeamRepository;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jantunea
 */
@ContextConfiguration(locations = "classpath:/spring/repository-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class GroupServiceTest {
    @Autowired
    Neo4jTemplate template;
    @Autowired
    GroupService groupService;
    
    @Before
    public void setup() throws Exception {
       template.query("START n = node(*) OPTIONAL MATCH n-[r]-() WHERE ID(n)>0 DELETE n, r", null);
    }
    
    @Autowired
    TeamRepository teamRepo;
    
    /////////////////////////////////////////////////////////////////////
    /// run this test e.g. 10 times, and you see that this succeed sometimes and
    /// sometimes it failes !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Test
    public void testRelatedInheritedMemberNotFetchedAlwaysCorrectly() throws Exception {
        Team team = createTestTeamNoIdSet();
        teamRepo.save(team);
        
        MemberGrouping grouping = new MemberGrouping();
        grouping.setName("coaches");
        
        //////////////////////////////////////
        // if you comment this out, there is no failure anymore
        ///////////////////////////////////////////////////
        grouping.setOwner(team);
        
        grouping.setMyGroup((GroupBase)team); // cast just to make look like it was GroupBase, propably no effect
        grouping = groupService.saveMemberGrouping(grouping);
        //assertEquals(1, grouping.getMemberIds().size());
        
        // TODO this tests that inherited entities are fetched correctly. 
        //sometimes fetched groupBase object fails to initialize correctly. -> see following asserts
        MemberGrouping finalTest = groupService.getMemberGrouping(grouping.getId());
        
         
        assertEquals(GroupBase.GroupType.TEAM, finalTest.getMyGroup().getGroupType()); // ok
        assertNotNull(((Team)grouping.getMyGroup()).getTeamName());  // fails sometimes, sometimes ok?!?
        
        //assertNotNull(((Team)grouping.getOwner()).getTeamName());
    }
    
    public static Team createTestTeamNoIdSet() {
        return createTestTeamNoIdSet("TestTeam");
    }
    
    public static Team createTestTeamNoIdSet(String name) {
        Team team = new Team();
        team.setPasswordToJoin("password");
        team.setTeamName("Team Name");
        team.setNameAcronym("Team Name");
        return team;
    }
}
