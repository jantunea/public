/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotNull;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.RelatedTo;


public class MemberGrouping extends EntityAuditInfo {
    
    @NotNull
    private String name;
    
    private Set<Long> memberIds;
    
    @RelatedTo(type="INTERNAL_GROUPING", direction = Direction.INCOMING)
    @Fetch
    @NotNull
    private GroupBase myGroup;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Long> getMemberIds() {
        if (memberIds == null) {
            memberIds = new HashSet<>();
        }
        return memberIds;
    }

    public void setMemberIds(Set<Long> memberIds) {
        this.memberIds = memberIds;
    }

    public GroupBase getMyGroup() {
        return myGroup;
    }

    public void setMyGroup(GroupBase myGroup) {
        this.myGroup = myGroup;
    }
    
}
