/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import javax.validation.constraints.NotNull;

/**
 *
 * @author jantunea
 */
public class SpecialMailAddr extends MailAddress {
    @NotNull
    private String special;

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }
    
    
}
