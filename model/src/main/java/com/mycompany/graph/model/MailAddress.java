/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import java.util.Set;
import javax.validation.constraints.NotNull;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

/**
 *
 * @author justmecoding
 */
@NodeEntity
public class MailAddress {
    @GraphId 
    private Long id;
    
    @Indexed(unique = true)
    @NotNull
    private String address;
    
    @RelatedTo(type = "MAIL_ADDRESS", direction = Direction.INCOMING)
    private User myUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getMyUsers() {
        return myUsers;
    }

    public void setMyUsers(User myUsers) {
        this.myUsers = myUsers;
    }
    
    
}
