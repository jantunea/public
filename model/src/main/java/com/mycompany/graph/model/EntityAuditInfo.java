/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import javax.validation.constraints.NotNull;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.RelatedTo;

/**
 * This class includes creator and owner information, among others
 *
 */
public abstract class EntityAuditInfo extends NodeEntityBase {
    //@NotNull
    @RelatedTo(type="NODE_CREATOR")
    User creator;
    
    /**
     * can be User or groupBase object
     */
    // TODO USER not used, change to GroupBase? user should have private group anyway, 
    //@NotNull
    @RelatedTo(type="NODE_OWNER")
    NodeEntityBase owner;
    
    
    boolean deleted = false;
    /**
     * Timestamp when deleted "softly"
     */
    @Indexed(numeric = true)
    Long deletedTime;

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public NodeEntityBase getOwner() {
        return owner;
    }

    public void setOwner(NodeEntityBase owner) {
        this.owner = owner;
    }

    public Long getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(Long deletedTime) {
        this.deletedTime = deletedTime;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
}
