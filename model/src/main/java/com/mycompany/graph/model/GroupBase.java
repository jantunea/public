/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import java.util.Set;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.RelatedTo;

public abstract class GroupBase extends NodeEntityBase {
    
    public enum GroupType {
        TEAM, CAR_POOL
    }

    /**
     * Every group must have name, but it does not need to be unique. (teamName is unique)
     */
    @NotNull
    @Size(min=2, max=256)
    private String name;
    
    @RelatedTo(type = "CHILD_GROUPS", direction = Direction.INCOMING)
    @Fetch
    private GroupBase parent;
    
    @RelatedTo(type = "INTERNAL_GROUPING")
    private Set<MemberGrouping> memberGroupings;
    
    @Size(min=6, max=512)
    private String passwordToJoin;
    
    @NotNull
    private GroupType groupType;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public GroupBase getParent() {
        return parent;
    }

    public void setParent(GroupBase parent) {
        this.parent = parent;
    }

    public Set<MemberGrouping> getMemberGroupings() {
        return memberGroupings;
    }

    public void setMemberGroupings(Set<MemberGrouping> memberGroupings) {
        this.memberGroupings = memberGroupings;
    }
    
    public String getPasswordToJoin() {
        return passwordToJoin;
    }

    public void setPasswordToJoin(String passwordToJoin) {
        this.passwordToJoin = passwordToJoin;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }
}
