/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import java.util.Set;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.RelatedTo;


public abstract class GroupComposite extends GroupBase {
    
    @RelatedTo(type = "CHILD_GROUPS", direction = Direction.OUTGOING)
    private Set<GroupBase> groupsUnder;
    
    public Set<GroupBase> getGroupsUnder() {
        return groupsUnder;
    }

    public void setGroupsUnder(Set<GroupBase> groupsUnder) {
        this.groupsUnder = groupsUnder;
    }
}
