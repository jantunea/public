/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import java.util.Set;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import static org.springframework.data.neo4j.support.index.IndexType.FULLTEXT;
import static org.springframework.data.neo4j.support.index.IndexType.SIMPLE;

/**
 * 
 * @author justmecoding
 */
@NodeEntity
public class User {
    @GraphId Long id;
    
    // SIMPLE used until SDN is updated. This makes SDN to throw exeption if uniqueness is violated (when using neo4j-rest).
    @Indexed(unique = true, indexType = SIMPLE)
    @NotNull
    @Size(min = 3, max = 128)
    private String username;
    
    /**
     * HASH of the password
     * if null can't login
     */
    @Size(min = 8)
    private String password;
    
    @Indexed
    @Size(min = 2, max = 30)
    @NotNull
    private String givenName;
    
    @Size(min = 2, max = 30)
    @NotNull
    @Indexed(indexName = "family-search", indexType = FULLTEXT)
    private String familyName;
    
    @RelatedTo(type = "MAIL_ADDRESS")
    @Fetch 
    private MailAddress mailAddress;
    
    private String mailVerificationString;
    
    private String sessionID;
    
    private long lastLogin;
    private long beforeLastLogin;
    @Indexed
    private int birthYear;
    private int birthMonth;
    private int birthDay;

    public User() {
    }

    /**
     * Creates user and makes random mail verification string.
     * @param username
     * @param password
     * @param givenName
     * @param familyName 
     */
    public User(String username, String password, String givenName, String familyName) {
        this.username = username;
        this.password = password;
        this.givenName = givenName;
        this.familyName = familyName;
        this.mailVerificationString = UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public MailAddress getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(MailAddress mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getMailVerificationString() {
        return mailVerificationString;
    }

    public void setMailVerified() {
        mailVerificationString = null;
    }
    
    public boolean isMailVerified() {
        if (mailVerificationString == null) {
            return true;
        }
        return false;
    }
    
    public void setMailVerificationString(String mailVerificationString) {
        this.mailVerificationString = mailVerificationString;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public long getBeforeLastLogin() {
        return beforeLastLogin;
    }

    public void setBeforeLastLogin(long beforeLastLogin) {
        this.beforeLastLogin = beforeLastLogin;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }
}
