/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.graph.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.neo4j.annotation.Indexed;
import static org.springframework.data.neo4j.support.index.IndexType.FULLTEXT;

public class Team extends GroupComposite {
    
    // Unique do not work with FULLTEXT, maybe some day?
    @Indexed(indexName = "team-search", indexType=FULLTEXT)
    @Size(min=2, max=256)
    @NotNull
    private String teamName;
    @Size(min=2, max=256)
    private String nameAcronym;
    private String headline;
    
    public Team() {
        setGroupType(GroupType.TEAM);
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
        setName(teamName);
    }

    public String getNameAcronym() {
        return nameAcronym;
    }

    public void setNameAcronym(String nameAcronym) {
        this.nameAcronym = nameAcronym;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }
}
